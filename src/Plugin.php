<?php
namespace Westhouseit\CraftCustomFees;

use craft\base\Plugin as CraftPlugin;
use craft\commerce\services\OrderAdjustments;
use craft\events\RegisterComponentTypesEvent;
use yii\base\Event;

class Plugin extends CraftPlugin
{
    public bool $hasCpSection = false;
    public bool $hasCpSettings = false;

    public function init()
    {
        parent::init();

        Event::on(
            OrderAdjustments::class,
            OrderAdjustments::EVENT_REGISTER_ORDER_ADJUSTERS,
            function(RegisterComponentTypesEvent $event) {
                $event->types = array_merge([FeeAdjuster::class], $event->types);
            }
        );
    }
    /**
     * Creates and returns the model used to store the plugin’s settings.
     */
    protected function createSettingsModel(): Settings
    {
        return new Settings();
    }
}
