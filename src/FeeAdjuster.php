<?php

namespace Westhouseit\CraftCustomFees;

use Craft;
use craft\base\Component;
use craft\commerce\base\AdjusterInterface;
use craft\commerce\elements\Order;
use craft\commerce\models\OrderAdjustment;

/*
 * TODO: Add support for Line Item conditions
 */

class FeeAdjuster extends Component implements AdjusterInterface
{
    public function adjust(Order $order): array
    {
        $adjustments = [];

        $fees = Plugin::getInstance()->getSettings()->fees;

        if (empty($fees)) {
            return $adjustments;
        }

        foreach ($fees as $fee) {
            Craft::debug(json_encode($fee));
            $applyFee = [];
            foreach ($fee['conditions'] as $condition) {
                $applyFee[] = Library::checkConditionApplies($condition, $order);
            }
            Craft::debug(json_encode($applyFee));
            // At the moment we're only using AND logic for conditions, so make sure all are true
            if (!in_array(false, $applyFee, true)) {
                $adjustment = new OrderAdjustment;
                $adjustment->type = 'fee';
                $adjustment->name = $fee['name'];
                $adjustment->description = $fee['description'];
                $adjustment->amount = $fee['amount'];
                $adjustment->setOrder($order);
                $adjustments[] = $adjustment;
            }
        }
        Craft::debug(json_encode($adjustments));
        return $adjustments;
    }
}
