<?php
namespace Westhouseit\CraftCustomFees;

use craft\commerce\elements\Order;

class Library
{
    public static array $validOperators = ['eq','ge','gt','le','lt','ne'];

    public static function validateCondition(array $condition, Order $order): void {
        if (!isset($order[$condition['orderField']])) {
            throw new Error(
                self::invalidFieldName($condition['orderField'])
            );
        }
        if (!in_array($condition['operator'], self::$validOperators)) {
            throw new Error(
                self::invalidOperator($condition['operator'])
            );
        }
    }

    public static function checkConditionApplies(array $condition, Order $order): bool {
        self::validateCondition($condition, $order);

        return match ($condition['operator']) {
            'eq' => $order[$condition['orderField']] === $condition['value'],
            'ge' => $order[$condition['orderField']] >= $condition['value'],
            'gt' => $order[$condition['orderField']] > $condition['value'],
            'le' => $order[$condition['orderField']] <= $condition['value'],
            'lt' => $order[$condition['orderField']] < $condition['value'],
            'ne' => $order[$condition['orderField']] !== $condition['value'],
            default => false,
        };
    }

    public static function invalidFieldName(string $field): string
    {
        return "Field name does not exist in this order: {$field}";
    }
    public static function invalidOperator(string $operator): string
    {
        return "Operator not valid: {$operator}. Valid operators are: ". implode(",", self::$validOperators);
    }
}
