<?php
namespace Westhouseit\CraftCustomFees;

use craft\base\Model;

class Settings extends Model
{
    // TODO: Work out how Craft allows using a settings object with getters/setters, rules, etc. while allowing an override file in config
    /*
     * Example fee: [
     *  'enabled' => true,
     *  'name' => '$1 booking fee',
     *  'description' => '$1 fee on all bookings',
     *  'amount' => 1,
     *  'conditions' => [],
     * ]
     */
	public array $fees = [];
}
