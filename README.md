# WIT Custom Fees plugin for Craft Commerce

## Installation

1.Add to your composer.json:

```
"repositories": [
  {
    "type": "vcs",
    "url":  "git@gitlab.com:westhouseit/craft-custom-fees"
  }
],
```

2. Run composer update
3. Install plugin via Craft control panel

## Configuration
In your Craft config directory add a file called wit-custom-fees.php with the content:

```
<?php

return [
    'fees' => [
            [
                'enabled' => true,
                'name' => 'Transaction fee',
                'description' => '£1 transaction fee on all orders under £100',
                'amount' => 1,
                'conditions' => [
                    [
                        'orderField'  => 'totalPrice',
                        'operator'    => 'lt',
                        'value'       => 100
                    ],
                ]
            ],
        ],
];
```

Any field that exists in the cart Object is support for the orderField option.

Supported comparison operators are:

| Operator | Description              |
|----------|--------------------------|
| eq       | Equal to                 |
| ge       | Greater than or equal to |
| gt       | Greater than             |
| le       | Less than or equal to    |
| lt       | Less than                |
| ne       | Not equal to             |

## Usage

```
{% for fee in cart.orderAdjustments|filter(adjustment => adjustment.type == "fee") %}
  {% if loop.first %}<div class="flex w-full justify-end items-center">{% endif %}
    <div class="pr-2">
      {{ fee.name|t }}:
    </div>
    <div>
      {{ fee.amountAsCurrency }}
    </div>
  {% if loop.last %}</div>{% endif %}
{% endfor %}
```
